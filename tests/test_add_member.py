import sys
import os

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(ROOT_DIR + os.sep)

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from pom.banner import Banner
from pom.home import Home
from pom.membership import Membership
from pom.main_cardholder import MainCardholder
from pom.submit_payment import SubmitPayment

CHROME_OSX_DRIVER = 'chromedriver_osx_2.40'
DRIVER_DIR = ROOT_DIR + os.sep + 'drivers' + os.sep

TEST_DATA = [
    {
        'first_name': 'John',
        'last_name': 'Doe',
        'gender': True,
        'different_mailing_address': False,
        'address': '26200 Enterprise Way',
        'city': 'Lake Forest',
        'state': 'CA',
        'zipcode': '92630',
        'email': 'foo@gmail.com',
        'email_preference': False,
        'phone_number': '1111111111',
        'is_phone_number_cell': False,
        'mobile_consent': False,
        'add_to_membership': False,
        'add_rv_motorcycle_roadside': False,
        'payment_method': True,
        'autopay_option': False
    },
    {
        'first_name': 'Jane',
        'last_name': 'Smith',
        'gender': False,
        'different_mailing_address': False,
        'address': '2601 S Figueroa St',
        'city': 'Los Angeles',
        'state': 'CA',
        'zipcode': '90007',
        'email': 'bar@gmail.com',
        'email_preference': False,
        'phone_number': '2222222222',
        'is_phone_number_cell': False,
        'mobile_consent': False,
        'add_to_membership': False,
        'add_rv_motorcycle_roadside': False,
        'payment_method': False,
        'autopay_option': False
    }
]


def test_1():
    """
    Test for adding a new female member.
    :return: None
    """
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--window-size=1920x1080')
    # browser = webdriver.Chrome(DRIVER_DIR + CHROME_OSX_DRIVER, chrome_options=chrome_options)
    browser = webdriver.Chrome(DRIVER_DIR + CHROME_OSX_DRIVER)
    browser.implicitly_wait(5)

    test_url = 'https://www.calif.aaa.com/'
    browser.get(test_url)

    zipcode_ban_obj = Banner(browser)
    # quick test to verify the appropriate text is displayed for the text field
    assert zipcode_ban_obj.get_zip_form_text() == 'Enter your ZIP Code'
    zipcode_ban_obj.set_text_for_zip_form(TEST_DATA[1]['zipcode'])
    zipcode_ban_obj.submit_zip_form()

    # quick test to verify page title
    assert 'Home' in browser.title
    home_obj = Home(browser)
    home_obj.click_join_aaa()

    # quick test to verify page title
    assert 'AAA - Select Level' in browser.title
    membership_obj = Membership(browser)
    membership_obj.click_get_started()

    main_cardholder_obj = MainCardholder(browser)
    # quick test to verify content on the page
    assert main_cardholder_obj.get_header_text() == 'Who is the primary member?'
    main_cardholder_obj.generic_insert_text('first_name', TEST_DATA[1]['first_name'])
    main_cardholder_obj.generic_insert_text('last_name', TEST_DATA[1]['last_name'])
    main_cardholder_obj.generic_select_between_two_options('gender', TEST_DATA[1]['gender'])
    main_cardholder_obj.generic_click('next_btn')

    main_cardholder_obj.generic_insert_text('address', TEST_DATA[1]['address'])
    main_cardholder_obj.generic_insert_text('city', TEST_DATA[1]['city'])
    main_cardholder_obj.generic_insert_text('state', TEST_DATA[1]['state'])
    main_cardholder_obj.generic_insert_text('zipcode', TEST_DATA[1]['zipcode'])
    main_cardholder_obj.generic_insert_text('email', TEST_DATA[1]['email'])
    main_cardholder_obj.generic_insert_text('phone_number', TEST_DATA[1]['phone_number'])
    main_cardholder_obj.generic_select_between_two_options('email_preference', TEST_DATA[1]['email_preference'])
    main_cardholder_obj.generic_select_between_two_options('is_phone_number_cell', TEST_DATA[1]['is_phone_number_cell'])
    main_cardholder_obj.generic_click('next_btn')

    main_cardholder_obj.generic_select_between_two_options('add_someone_to_membership',
                                                           TEST_DATA[1]['add_to_membership'])
    main_cardholder_obj.generic_select_between_two_options('add_rv_motorcyle_roadside',
                                                           TEST_DATA[1]['add_rv_motorcycle_roadside'])

    main_cardholder_obj.generic_click('checkout_btn')

    submit_payment_obj = SubmitPayment(browser)
    submit_payment_obj.generic_click('payment_method')
    submit_payment_obj.generic_select_between_two_options('payment_method_type', TEST_DATA[1]['payment_method'])
    submit_payment_obj.generic_click('close_payment_modal')
    submit_payment_obj.generic_click('submit_payment_btn')
    assert submit_payment_obj.generic_is_visible('missing_payment_error')


def test_2():
    """
    Test for adding a new male member.
    :return: None
    """
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--window-size=1920x1080')
    # browser = webdriver.Chrome(DRIVER_DIR + CHROME_OSX_DRIVER, chrome_options=chrome_options)
    browser = webdriver.Chrome(DRIVER_DIR + CHROME_OSX_DRIVER)
    browser.implicitly_wait(5)

    test_url = 'https://www.calif.aaa.com/'
    browser.get(test_url)

    zipcode_ban_obj = Banner(browser)
    # quick test to verify the appropriate text is displayed for the text field
    assert zipcode_ban_obj.get_zip_form_text() == 'Enter your ZIP Code'
    zipcode_ban_obj.set_text_for_zip_form(TEST_DATA[0]['zipcode'])
    zipcode_ban_obj.submit_zip_form()

    # quick test to verify page title
    assert 'Home' in browser.title
    home_obj = Home(browser)
    home_obj.click_join_aaa()

    # quick test to verify page title
    assert 'AAA - Select Level' in browser.title
    membership_obj = Membership(browser)
    membership_obj.click_get_started()

    main_cardholder_obj = MainCardholder(browser)
    # quick test to verify content on the page
    assert main_cardholder_obj.get_header_text() == 'Who is the primary member?'
    main_cardholder_obj.generic_insert_text('first_name', TEST_DATA[0]['first_name'])
    main_cardholder_obj.generic_insert_text('last_name', TEST_DATA[0]['last_name'])
    main_cardholder_obj.generic_select_between_two_options('gender', TEST_DATA[0]['gender'])
    main_cardholder_obj.generic_click('next_btn')

    main_cardholder_obj.generic_insert_text('address', TEST_DATA[0]['address'])
    main_cardholder_obj.generic_insert_text('city', TEST_DATA[0]['city'])
    main_cardholder_obj.generic_insert_text('state', TEST_DATA[0]['state'])
    main_cardholder_obj.generic_insert_text('zipcode', TEST_DATA[0]['zipcode'])
    main_cardholder_obj.generic_insert_text('email', TEST_DATA[0]['email'])
    main_cardholder_obj.generic_insert_text('phone_number', TEST_DATA[0]['phone_number'])
    main_cardholder_obj.generic_select_between_two_options('email_preference', TEST_DATA[0]['email_preference'])
    main_cardholder_obj.generic_select_between_two_options('is_phone_number_cell', TEST_DATA[0]['is_phone_number_cell'])
    main_cardholder_obj.generic_click('next_btn')

    main_cardholder_obj.generic_select_between_two_options('add_someone_to_membership',
                                                           TEST_DATA[0]['add_to_membership'])
    main_cardholder_obj.generic_select_between_two_options('add_rv_motorcyle_roadside',
                                                           TEST_DATA[0]['add_rv_motorcycle_roadside'])

    main_cardholder_obj.generic_click('checkout_btn')

    submit_payment_obj = SubmitPayment(browser)
    submit_payment_obj.generic_click('payment_method')
    submit_payment_obj.generic_select_between_two_options('payment_method_type', TEST_DATA[0]['payment_method'])
    submit_payment_obj.generic_click('close_payment_modal')
    submit_payment_obj.generic_click('submit_payment_btn')
    assert submit_payment_obj.generic_is_visible('missing_payment_error')
