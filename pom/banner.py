from selenium.webdriver.common.by import By
from selenium.common import exceptions
from selenium.webdriver.remote.webdriver import WebElement
from selenium.webdriver.remote.webdriver import WebDriver


class Banner(object):
    def __init__(self, browser_driver):
        self.driver = browser_driver
        self.zip_prompt = (By.CLASS_NAME, 'zip-prompt')
        self.zip_field = (By.XPATH, "//input[@name='zip-code']")
        self.zip_form_submit_btn = (By.XPATH, "//div[@class='modal-content']/a")

    def get_zip_form_text(self):
        try:
            elem = self.driver.find_element(by=self.zip_prompt[0], value=self.zip_prompt[1])
            if elem:
                return elem.text
            else:
                raise exceptions.WebDriverException
        except:
            self.driver.close()

    def set_text_for_zip_form(self, zipcode):
        try:
            elem = self.driver.find_element(by=self.zip_field[0], value=self.zip_field[1])
            if elem:
                elem.send_keys(zipcode)
            else:
                raise exceptions.WebDriverException
        except:
            self.driver.close

    def submit_zip_form(self):
        try:
            self.driver.find_element(by=self.zip_form_submit_btn[0], value=self.zip_form_submit_btn[1]).click()
        except:
            self.driver.close
