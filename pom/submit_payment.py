from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

TIMEOUT = 5


class SubmitPayment(object):
    def __init__(self, browser_driver):
        self.driver = browser_driver
        self.payment_method = (By.XPATH, "//button[@type='button'][@id='PaymentMethod']")
        self.payment_method_type = [
            (By.XPATH, "//a[contains(text(),'New Credit Card')]"),
            (By.XPATH, "//a[contains(text(),'New Debit Card')]")
        ]
        self.close_payment_modal = (By.XPATH, "//div[@class='modal-content']//span[@class='close']")
        self.autopay_option = [(By.ID, 'AutoPayYes'), (By.ID, 'AutoPayNo')]
        self.submit_payment_btn = (By.XPATH, "//input[@type='submit'][@id='paymentSubmitBtn']")
        self.missing_payment_error = (By.XPATH,
                                      "//div[@class='errorContent'][contains(text(),'Payment Method is required.')]")

    def generic_is_visible(self, class_attr):
        """
        A simple but generic method to determine if a web element is visible on the page.
        :param class_attr: a class attribute found within 'SubmitPayment'
        :return: True when visible otherwise, throw an exception
        """
        try:
            if hasattr(self, class_attr):
                attribute_value = getattr(self, class_attr)
                WebDriverWait(self.driver, TIMEOUT).until(EC.visibility_of_element_located(attribute_value)).click()
                return True
            else:
                raise AttributeError
        except:
            raise

    def generic_select_between_two_options(self, class_attr, option=True):
        """
        A simple but generic method to select between two options.
        :param class_attr: a class attribute found within 'SubmitPayment'
        :param option: True to select the first options, otherwise select the second option
        :return: None
        """
        try:
            if hasattr(self, class_attr):
                attribute_value = getattr(self, class_attr)
                if option:
                    radio_btn = attribute_value[0]
                else:
                    radio_btn = attribute_value[1]
                self.driver.find_element(by=radio_btn[0], value=radio_btn[1]).click()
            else:
                raise AttributeError
        except:
            raise

    def generic_insert_text(self, class_attr, input_data):
        """
        A simple but generic method to insert text into an input field.
        :param class_attr: a class attribute found within 'SubmitPayment'
        :param input_data: the text to be entered
        :return: None
        """
        try:
            if hasattr(self, class_attr):
                attribute_value = getattr(self, class_attr)
                self.driver.find_element(by=attribute_value[0], value=attribute_value[1]).send_keys(input_data)
            else:
                raise AttributeError
        except:
            raise

    def generic_click(self, class_attr):
        """
        A simple but generic method to click on web elements.
        :param class_attr: a class attribute found within 'SubmitPayment'
        :return: None
        """
        try:
            if hasattr(self, class_attr):
                attribute_value = getattr(self, class_attr)
                WebDriverWait(self.driver, TIMEOUT).until(EC.element_to_be_clickable(attribute_value)).click()
            else:
                raise AttributeError
        except:
            raise
