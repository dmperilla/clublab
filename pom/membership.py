from selenium.webdriver.common.by import By


class Membership(object):
    def __init__(self, browser_driver):
        self.driver = browser_driver
        self.get_started_btn = (By.ID, 'classicSelectButtonTop')

    def click_get_started(self):
        self.driver.find_element(by=self.get_started_btn[0], value=self.get_started_btn[1]).click()
