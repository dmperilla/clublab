from selenium.webdriver.common.by import By
from selenium.common import exceptions
from selenium.webdriver.remote.webdriver import WebElement
from selenium.webdriver.remote.webdriver import WebDriver


class Home(object):
    def __init__(self, browser_driver):
        self.driver = browser_driver
        self.join_aaa_btn = (By.LINK_TEXT, 'JOIN AAA')

    def click_join_aaa(self):
        self.driver.find_element(by=self.join_aaa_btn[0], value=self.join_aaa_btn[1]).click()
