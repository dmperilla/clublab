from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

TIMEOUT = 5


class MainCardholder(object):
    def __init__(self, browser_driver):
        self.driver = browser_driver
        self.header = (By.XPATH, "//h4[@class='primary-member-title']")
        self.first_name = (By.ID, 'firstName')
        self.last_name = (By.ID, 'lastName')
        self.gender = [(By.ID, 'optionsRadios1'), (By.ID, 'optionsRadios2')]
        self.address = (By.ID, 'exampleInputAddress')
        self.city = (By.ID, 'cityList')
        self.state = (By.ID, 'state')
        self.zipcode = (By.ID, 'ZIP')
        self.email = (By.ID, 'memberEmail')
        self.email_preference = [
            (By.XPATH, "//input[@type='radio'][@name='contactInfoIsReceiveEmails'][@id='optionsRadios1']"),
            (By.XPATH, "//input[@type='radio'][@name='contactInfoIsReceiveEmails'][@id='optionsRadios2']")
        ]
        self.phone_number = (By.ID, 'exampleInputPhonenum')
        self.is_phone_number_cell = [
            (By.XPATH, "//input[@type='radio'][@name='contactInfoIsCellPhone'][@id='optionsRadios1']"),
            (By.XPATH, "//input[@type='radio'][@name='contactInfoIsCellPhone'][@id='optionsRadios2']")
        ]
        self.mobile_consent = [
            (By.XPATH, "//input[@type='radio'][@name='contactInfoIsAgreement'][@id='optionsRadios1']"),
            (By.XPATH, "//input[@type='radio'][@name='contactInfoIsAgreement'][@id='optionsRadios2']")
        ]
        self.add_someone_to_membership = [
            (By.XPATH, "//button[@type='button'][@id='yesToAddAssociateButton']"),
            (By.XPATH, "//button[@type='button'][@id='noThanksToAddAssociateButton']")
        ]
        self.add_rv_motorcyle_roadside = [
            (By.XPATH, "//button[@type='button'][@id='yesToAddOptionsButton']"),
            (By.XPATH, "//button[@type='button'][@id='noToAddOptionsLink']")
        ]
        self.next_btn = (By.XPATH, "//button[contains(text(),'Next')]")
        self.checkout_btn = (
            By.XPATH, "//a[@type='button'][@id='proceedToCheckoutButton'][contains(text(),'Proceed to checkout')]")

    def generic_select_between_two_options(self, class_attr, option=True):
        """
        A simple but generic method to select between two options.
        :param class_attr: a class attribute found within 'MainCardholder'
        :param option: True to select the first options, otherwise select the second option
        :return: None
        """
        try:
            if hasattr(self, class_attr):
                attribute_value = getattr(self, class_attr)
                if option:
                    radio_btn = attribute_value[0]
                else:
                    radio_btn = attribute_value[1]
                self.driver.find_element(by=radio_btn[0], value=radio_btn[1]).click()
            else:
                raise AttributeError
        except:
            raise

    def generic_insert_text(self, class_attr, input_data):
        """
        A simple but generic method to insert text into an input field.
        :param class_attr: a class attribute found within 'MainCardholder'
        :param input_data: the text to be entered
        :return: None
        """
        try:
            if hasattr(self, class_attr):
                attribute_value = getattr(self, class_attr)
                self.driver.find_element(by=attribute_value[0], value=attribute_value[1]).send_keys(input_data)
            else:
                raise AttributeError
        except:
            raise

    def generic_click(self, class_attr):
        """
        A simple but generic method to click on web elements.
        :param class_attr: a class attribute found within 'MainCardholder'
        :return: None
        """
        try:
            if hasattr(self, class_attr):
                attribute_value = getattr(self, class_attr)
                WebDriverWait(self.driver, TIMEOUT).until(EC.element_to_be_clickable(attribute_value)).click()
            else:
                raise AttributeError
        except:
            raise

    def get_header_text(self):
        """
        Return the text pertaining to a predefined header. (self.header)
        :return: Header text (str)
        """
        return self.driver.find_element(by=self.header[0], value=self.header[1]).text
