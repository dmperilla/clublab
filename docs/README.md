# ClubLab Exercise

### Instructions
 1. Go to <https://www.calif.aaa.com/>
 2. Select `Join AAA`
 3. Select membership (only one option)
 4. Fulfil all required forms:
    - member info
    - contact details
    - additional members
    - membership options
 5. Go to payment page
 6. Open payment method window and close it
 7. Submit payment
 8. Verify error appears, “Payment Method is required”

 
### Requirements
 1. Make automated test that covers at least 2 users: 
    - male
    - female
 2. Use any tools in addition to Selenium, any language, your preference.
 
### Prerequisites for launching the exercise
1. OSX
2. Chrome v67.x.xxxx.xx (64-bit)
3. Python3.6 
 
### Instructions for running the exercise
 ```bash
$ pip install -r requirements.txt -U --no-cache-dir
$ pytest
```

sample output:
```bash
$ pytest
==================================================================================== test session starts ====================================================================================
platform darwin -- Python 3.6.5, pytest-3.6.3, py-1.5.4, pluggy-0.6.0 -- /Users/perillad/Desktop/python_workspace/clublab/bin/python
cachedir: .pytest_cache
rootdir: /Users/perillad/Documents/PyCharm_Workspace/clublab, inifile: pytest.ini
collected 2 items                                                                                                                                                                           

tests/test_add_member.py::test_1 PASSED                                                                                                                                               [ 50%]
tests/test_add_member.py::test_2 PASSED                                                                                                                                               [100%]

================================================================================= 2 passed in 44.10 seconds =================================================================================
```
### About
Language: Developed with `Python3.6`  
Author: Diego M. Perilla
